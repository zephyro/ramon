<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_production">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Салон Ramon Performance г. Ярославль</li>
                            </ul>
                            <h1>Салон Ramon Performance г. Ярославль</h1>
                        </div>

                    </div>

                    <div class="unit">

                        <div class="row">
                            <div class="col-xs-12 col-lg-6">
                                <h2>Описание салона</h2>
                                <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло. Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами.</p>
                            </div>
                            <div class="col-xs-12 col-lg-6">
                                <div class="unit_gallery">
                                    <div class="unit_slider swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="unit_thumbs">
                                        <li><a href="#" data-target="0"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="1"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="2"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="3"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="4"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="5"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="6"><i class="fa fa-camera"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="builder">
                        <h2 class="text-center">Конструктор кованых дисков</h2>
                        <div class="row">
                            <div class="col-xs-12 col-lg-4 col-xl-3">

                                <div class="builder__step">
                                    <ol>
                                        <li class="active">Выберите дизайн</li>
                                        <li>Выберите комплектацию</li>
                                        <li>Заполните форму</li>
                                    </ol>
                                </div>

                            </div>
                            <div class="col-xs-12 col-lg-8 col-xl-9">

                                <div class="builder__content">
                                    <div class="builder__content_image">
                                        <img src="images/whell_lg.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="builder__params">
                                        <div class="builder__params_row">
                                            <div class="builder__params_col">
                                                <div class="form_group">
                                                    <select class="select_white">
                                                        <option value="Mercedes-Benz" selected>Ширина</option>
                                                        <option value="Mercedes-Benz">10</option>
                                                        <option value="Volkswagen">50</option>
                                                        <option value="BMW">100</option>
                                                        <option value="Audi">120</option>
                                                    </select>
                                                </div>
                                                <div class="form_group">
                                                    <div class="form_group">
                                                        <select class="select_white">
                                                            <option value="Mercedes-Benz" selected>Вылет</option>
                                                            <option value="Mercedes-Benz">10</option>
                                                            <option value="Volkswagen">50</option>
                                                            <option value="BMW">100</option>
                                                            <option value="Audi">120</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form_group">
                                                    <select class="select_white">
                                                        <option value="Mercedes-Benz" selected>Кол-во болтов</option>
                                                        <option value="Mercedes-Benz">10</option>
                                                        <option value="Volkswagen">50</option>
                                                        <option value="BMW">100</option>
                                                        <option value="Audi">120</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="builder__params_col">
                                                <div class="form_group">
                                                    <select class="select_white">
                                                        <option value="Mercedes-Benz" selected>Диаметр</option>
                                                        <option value="Mercedes-Benz">10</option>
                                                        <option value="Volkswagen">50</option>
                                                        <option value="BMW">100</option>
                                                        <option value="Audi">120</option>
                                                    </select>
                                                </div>
                                                <div class="form_group">
                                                    <select class="select_white">
                                                        <option value="Mercedes-Benz" selected>Отверстие</option>
                                                        <option value="Mercedes-Benz">10</option>
                                                        <option value="Volkswagen">50</option>
                                                        <option value="BMW">100</option>
                                                        <option value="Audi">120</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="builder__params_col">
                                                <div class="form_group">
                                                    <select class="select_white">
                                                        <option value="Mercedes-Benz" selected>Цвет</option>
                                                        <option value="Mercedes-Benz">Цвет 1</option>
                                                        <option value="Volkswagen">Цвет 2</option>
                                                        <option value="BMW">Цвет 3</option>
                                                        <option value="Audi">Цвет 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="builder__params_col">
                                                <div class="form_group">
                                                    <select class="select_white">
                                                        <option value="Mercedes-Benz" selected>Цвет</option>
                                                        <option value="Mercedes-Benz">Цвет 1</option>
                                                        <option value="Volkswagen">Цвет 2</option>
                                                        <option value="BMW">Цвет 3</option>
                                                        <option value="Audi">Цвет 4</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="text-right">
                                            <a href="#" class="btn btn_red">Продолжить <i class="fa fa-angle-double-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
