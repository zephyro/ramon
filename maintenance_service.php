<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page maintenance_bg">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Техническое обслуживание</li>
                    </ul>

                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <h1>Техническое обслуживание</h1>
                            <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову,</p>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6 col-xl-5 col-xl-offset-1">
                            <div class="main_video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe  src="https://www.youtube.com/embed/9iQL87myauE?rel=0&amp;showinfo=0" frameborder="0" encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br/>
                    <br/>

                    <h2>An Introduction</h2>
                    <p>This little baseline has (almost) all of the content flow and phrasing elements. It attempts to use all of those elements according to their defined semantics. It also tries to provide a baseline style for those elements. So this isn't a normalize or a reset, but maybe the first set of styles you'd add before you start branding stuff. Here is the full list of the elements.</p>
                    <br/>

                    <h3>Definitions</h3>
                    <p>I'm that paragraph with some emphasis on the text-level semantics where I might feel the need to share some strong opinions. This paragraph even uses elements that should have been deprecated, but instead the W3C redefined them. And that's dumb, because formerly presentational elements now do silly things like call a thing to your attention for no good reason, or tell you that something is mispelled, or the name of a boat — which makes as much sense as building another Titanic. It makes no sense, but … c'est la vie.</p>
                    <p>I'm that paragraph with some emphasis on the text-level semantics where I might feel the need to share some strong opinions. This paragraph even uses elements that should have been deprecated, but instead the W3C redefined them. And that's dumb, because formerly presentational elements now do silly things like call a thing to your attention for no good reason, or tell you that something is mispelled, or the name of a boat — which makes as much sense as building another Titanic. It makes no sense, but … c'est la vie.</p>

                    <h3>Text Level Semantics</h3>
                    <p>I'm that paragraph with some emphasis on the text-level semantics where I might feel the need to share some strong opinions. This paragraph even uses elements that should have been deprecated, but instead the W3C redefined them. And that's dumb, because formerly presentational elements now do silly things like call a thing to your attention for no good reason, or tell you that something is mispelled, or the name of a boat — which makes as much sense as building another Titanic. It makes no sense, but … c'est la vie.</p>
                    <p>I'm that paragraph with some emphasis on the text-level semantics where I might feel the need to share some strong opinions. This paragraph even uses elements that should have been deprecated, but instead the W3C redefined them. And that's dumb, because formerly presentational elements now do silly things like call a thing to your attention for no good reason, or tell you that something is mispelled, or the name of a boat — which makes as much sense as building another Titanic. It makes no sense, but … c'est la vie.</p>



                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
