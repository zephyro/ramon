<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>Каталог</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Каталог</li>
                            </ul>

                            <h1>Каталог</h1>
                        </div>

                    </div>

                    <div class="main_filter">
                        <div class="form_title">Фильтрация каталога</div>
                        <div class="main_filter_wrap">
                            <div class="row">
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Марка</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Модель</option>
                                            <option value="GLE-500">GLE-500</option>
                                            <option value="SLK-500">SLK-500</option>
                                            <option value="GL-200">GL-200</option>
                                            <option value="GLA">GLA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Категория</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Бренд</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col text-center">
                                    <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Models -->
                    <?php include('inc/models.inc.php') ?>
                    <!-- -->

                    <!-- Pagination -->
                    <?php include('inc/pagination.inc.php') ?>
                    <!-- -->

                </div>
            </section>

            <div class="service_block">
                <div class="service_item item_02">
                    <div class="service_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_03">
                    <div class="service_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
