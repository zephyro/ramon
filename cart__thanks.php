<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>
                    </div>

                    <div class="order">
                        <div class="h1 text-center">Спасибо за покупку!</div>
                        <div class="subtitle">Наш менеджер свяжется с вами в ближайшее время.</div>

                        <div class="order_info">
                            <h2>Информация о заказе</h2>
                            <ul>

                                <li>Номер заказа: <strong>600</strong></li>
                                <li>Дата: <strong>07.06.2018</strong></li>
                                <li>E-mail: <strong>client@yandex.ru</strong></li>
                                <li>Всего: <strong>2 800 000 Р</strong></li>
                                <li>Способ оплаты: <strong>Прямой банковский перевод</strong></li>
                            </ul>
                        </div>

                        <div class="order_info">
                            <h2>Состав заказа</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Column One</th>
                                        <th>Количество</th>
                                        <th>Итого</th>
                                    </tr>
                                    <tr>
                                        <td>Alexander Stokes</td>
                                        <td>4</td>
                                        <td>2 800 000 Р</td>
                                    </tr>
                                    <tr>
                                        <td>Tom Jenkins</td>
                                        <td>12</td>
                                        <td>15 000 000 Р</td>
                                    </tr>
                                    <tr class="table_summary">
                                        <td colspan="2">Подитог</td>
                                        <td>17 800 000 Р</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="order_info">
                                <h2>Платёжный адрес</h2>
                                <p>
                                    Российская Федерация<br/>
                                    Центральный Федеральный Округ<br/>
                                    Ярославская обл.<br/>
                                    Г. Ярославль<br/>
                                    Ул. Андропова, д 11, кв 17
                                </p>
                            </div>

                            <div class="order_info">
                                <h2>Адрес доставки</h2>
                                <p>
                                    Российская Федерация<br/>
                                    Центральный Федеральный Округ<br/>
                                    Ярославская обл.<br/>
                                    Г. Ярославль<br/>
                                    Ул. Андропова, д 11, кв 17
                                </p>
                            </div>

                            <ul class="btn_group">
                                <li>
                                    <a href="#" class="btn btn_animate btn_red" data-text="На главную"><span>На главную</span></a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn_animate btn_dark_border" data-text="Каталог"><span>Каталог</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>

                </div>
            </section>


            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
