<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Статьи</li>
                            </ul>

                            <h1>Статьи</h1>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-lg-8 col-xl-8">

                            <article class="article">
                                <div class="article__header">
                                    <div class="article__title">
                                        <div class="article__title_prime"><a href="#">Шикарная новость</a></div>
                                        <div class="article__title_second">Подзаголовок шикарной новости</div>
                                    </div>
                                    <div class="article__date">28.04.2018</div>
                                </div>
                                <div class="article__image">
                                    <a href="#">
                                        <img src="images/article_image_01.jpg" class="img-fluid" alt="Шикарная новость">
                                    </a>
                                </div>
                                <div class="article__intro">
                                    Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло.
                                </div>
                                <ul class="article__footer">
                                    <li>
                                        <div class="article_view"><i class="fa fa-eye"></i> 5089</div>
                                    </li>
                                    <li>
                                        <a href="#" class="btn"><span>Читать далее <i class="fa fa-angle-double-right"></i></span></a>
                                    </li>
                                </ul>
                            </article>

                            <article class="article">
                                <div class="article__header">
                                    <div class="article__title">
                                        <div class="article__title_prime"><a href="#">Шикарная новость</a></div>
                                        <div class="article__title_second">Подзаголовок шикарной новости</div>
                                    </div>
                                    <div class="article__date">28.04.2018</div>
                                </div>
                                <div class="article__image">
                                    <a href="#">
                                        <img src="images/article_image_01.jpg" class="img-fluid" alt="Шикарная новость">
                                    </a>
                                </div>
                                <div class="article__intro">
                                    Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло.
                                </div>
                                <ul class="article__footer">
                                    <li>
                                        <div class="article_view"><i class="fa fa-eye"></i> 5089</div>
                                    </li>
                                    <li>
                                        <a href="#" class="btn">Читать далее <i class="fa fa-angle-double-right"></i></a>
                                    </li>
                                </ul>
                            </article>

                            <article class="article">
                                <div class="article__header">
                                    <div class="article__title">
                                        <div class="article__title_prime"><a href="#">Шикарная новость</a></div>
                                        <div class="article__title_second">Подзаголовок шикарной новости</div>
                                    </div>
                                    <div class="article__date">28.04.2018</div>
                                </div>
                                <div class="article__image">
                                    <a href="#">
                                        <img src="images/article_image_01.jpg" class="img-fluid" alt="Шикарная новость">
                                    </a>
                                </div>
                                <div class="article__intro">
                                    Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло.
                                </div>
                                <ul class="article__footer">
                                    <li>
                                        <div class="article_view"><i class="fa fa-eye"></i> 5089</div>
                                    </li>
                                    <li>
                                        <a href="#" class="btn">Читать далее <i class="fa fa-angle-double-right"></i></a>
                                    </li>
                                </ul>
                            </article>

                        </div>
                        <div class="col-xs-12 col-lg-4 col-xl-4">
                            <div class="side_box">
                                <div class="article__nav">
                                    <div class="h3">Другие статьи</div>
                                    <ul>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="side_box">
                                <div class="side_box_title">
                                    <div class="h3">Собственное производство</div>
                                </div>

                                <div class="side_product side_product_one">
                                    <div class="side_product_title">Обвесы</div>
                                    <a href="#" class="btn btn_animate btn_dark_border" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>

                                <div class="side_product side_product_two">
                                    <div class="side_product_title">Колесные диски</div>
                                    <a href="#" class="btn btn_animate btn_dark_border" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>
                            </div>

                            <div class="side_box">
                                <div class="actions__item item_01">
                                    <h4><span>Подарочный сертификат</span></h4>
                                    <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>
                            </div>

                            <div class="side_box">
                                <div class="actions__item item_02">
                                    <h4><span>Скидка 20%<br/> на все выхлопные системы</span></h4>
                                    <p>с 1 марта по 1 апреля</p>
                                    <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>


            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
