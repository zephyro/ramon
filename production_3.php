<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="production">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Производство</li>
                    </ul>

                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <h1>Производство карбоновых обвесов</h1>
                            <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову,</p>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6 col-xl-5 col-xl-offset-1">
                            <div class="main_video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe  src="https://www.youtube.com/embed/9iQL87myauE?rel=0&amp;showinfo=0" frameborder="0" encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <ul class="portfolio_block">
                <li style="background-image: url('images/productions/pr_01.jpg')"><img src="images/productions/pr_01.jpg" class="img-fluid" alt=""></li>
                <li style="background-image: url('images/productions/pr_02.jpg')"></li>
                <li style="background-image: url('images/productions/pr_03.jpg')"></li>
                <li style="background-image: url('images/productions/pr_04.jpg')"></li>
                <li style="background-image: url('images/productions/pr_05.jpg')"></li>
                <li style="background-image: url('images/productions/pr_06.jpg')"></li>
                <li style="background-image: url('images/productions/pr_07.jpg')"></li>
                <li style="background-image: url('images/productions/pr_08.jpg')"></li>
            </ul>

            <div class="order_block">
                <div class="container">

                    <div class="form_inline">
                        <div class="form_title">Заказать обвес</div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Модель</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Двигатель</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 text-center">
                                <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                            </div>
                        </div>
                    </div>

                    <div class="position">
                        <label class="position_item">
                            <input type="checkbox" value="" checked>
                            <div class="position_elem">
                                <img src="img/auto_front_01.png" class="img-fluid" alt="">
                                <span>Передний бампер</span>
                            </div>
                        </label>
                        <label class="position_item">
                            <input type="checkbox" value="">
                            <div class="position_elem">
                                <img src="img/auto_front_02.png" class="img-fluid" alt="">
                                <span>Задний бампер</span>
                            </div>
                        </label>
                        <label class="position_item">
                            <input type="checkbox" value="">
                            <div class="position_elem">
                                <img src="img/auto_profile.png" class="img-fluid" alt="">
                                <span>Порог</span>
                            </div>
                        </label>
                        <label class="position_item">
                            <input type="checkbox" value="">
                            <div class="position_elem">
                                <img src="img/auto_back_01.png" class="img-fluid" alt="">
                                <span>Капот</span>
                            </div>
                        </label>
                        <label class="position_item">
                            <input type="checkbox" value="">
                            <div class="position_elem">
                                <img src="img/auto_back_02.png" class="img-fluid" alt="">
                                <span>Спойлер</span>
                            </div>
                        </label>
                    </div>

                    <div class="builder__form">
                        <form class="form">
                            <div class="form_group">
                                <input type="text" class="form_control_base" placeholder="Ваше имя">
                            </div>
                            <div class="form_group">
                                <input type="text" class="form_control_base" placeholder="Телефон">
                            </div>
                            <div class="form_group">
                                <input type="text" class="form_control_base" placeholder="E-mail">
                            </div>
                            <div class="form_group">
                                <textarea class="form_control_base" placeholder="Комментарий к заказу" name="" rows="5"></textarea>
                            </div>
                            <div class="form_group">
                                <div class="checkbox_group">
                                    <div class="checkbox_col">
                                        <div class="checkbox_group_title">Пункт выдачи:</div>
                                    </div>
                                    <div class="checkbox_col">
                                        <label class="radiobutton">
                                            <input type="radio" name="nn" value="" checked>
                                            <div class="radiobutton_text">
                                                <span>Москва</span>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="checkbox_col">
                                        <label class="radiobutton">
                                            <input type="radio" name="nn" value="">
                                            <div class="radiobutton_text">
                                                <span>Ярославль</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form_submit">
                                <div class="form_submit_captcha">
                                    <img src="images/captcha.png" class="img-fluid" alt="">
                                </div>
                                <div class="form_submit_btn">
                                    <button type="submit" class="btn btn_animate btn_red" data-text="Отправить заявку"><span>Отправить заявку</span></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <section class="main_page">
                <div class="container">

                    <!-- Vendors -->
                    <?php include('inc/vendors.inc.php') ?>
                    <!-- -->

                    <!-- Pagination -->
                    <?php include('inc/pagination.inc.php') ?>
                    <!-- -->
                </div>
            </section>

            <div class="service_block">
                <div class="service_item item_02">
                    <div class="service_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_03">
                    <div class="service_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Content -->
            <?php include('inc/content.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>


        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
