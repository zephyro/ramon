<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Корзина</li>
                            </ul>

                            <h1>Корзина</h1>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-7 col-lg-9 col-xl-9">

                            <div class="cart">
                                <ul class="cart__head">
                                    <li>товар</li>
                                    <li>цена</li>
                                    <li>кол-во</li>
                                    <li>подитог</li>
                                </ul>
                                <ul class="cart__row">
                                    <li>
                                        <a href="#" class="cart_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a class="cart_name" href="#">Audi RS 7 Sportback (C7) 2017</a>
                                    </li>
                                    <li>
                                        <span class="cart_price">2 000 000 Р</span>
                                    </li>
                                    <li>
                                        <div class="form_num">
                                            <span class="form_num_down"><i class="fa fa-minus"></i></span>
                                            <input type="text" name="num" value="1" class="form_num_input">
                                            <span class="form_num_up"><i class="fa fa-plus"></i></span>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="cart_price">2 000 000 Р</span>
                                    </li>
                                    <li>
                                        <span class="cart_remove">X</span>
                                    </li>
                                </ul>
                                <ul class="cart__row">
                                    <li>
                                        <a href="#" class="cart_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a class="cart_name" href="#">Audi RS 7 Sportback (C7) 2017</a>
                                    </li>
                                    <li>
                                        <span class="cart_price">2 000 000 Р</span>
                                    </li>
                                    <li>
                                        <div class="form_num">
                                            <span class="form_num_down"><i class="fa fa-minus"></i></span>
                                            <input type="text" name="num" value="1" class="form_num_input">
                                            <span class="form_num_up"><i class="fa fa-plus"></i></span>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="cart_price">2 000 000 Р</span>
                                    </li>
                                    <li>
                                        <span class="cart_remove">X</span>
                                    </li>
                                </ul>
                                <ul class="cart__row">
                                    <li>
                                        <a href="#" class="cart_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a class="cart_name" href="#">Audi RS 7 Sportback (C7) 2017</a>
                                    </li>
                                    <li>
                                        <span class="cart_price">2 000 000 Р</span>
                                    </li>
                                    <li>
                                        <div class="form_num">
                                            <span class="form_num_down"><i class="fa fa-minus"></i></span>
                                            <input type="text" name="num" value="1" class="form_num_input">
                                            <span class="form_num_up"><i class="fa fa-plus"></i></span>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="cart_price">2 000 000 Р</span>
                                    </li>
                                    <li>
                                        <span class="cart_remove">X</span>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-xs-12 col-md-5 col-lg-3 col-xl-3">
                            <div class="summary">
                                <div class="summary_title">Итог:</div>
                                <div class="summary_value">800 000 000 Р</div>
                                <div class="form_group">
                                    <button type="submit" class="btn btn_animate btn_red" data-text="К оплате"><span>К оплате</span></button>
                                </div>
                                <div class="form_group">
                                    <button type="submit" class="btn btn_animate btn_white_border" data-text="Вернуться к покупкам"><span>Вернуться к покупкам</span></button>
                                </div>
                                <ul class="summary_info">
                                    <li><a href="#">Информация о доставке</a></li>
                                    <li><a href="#">Информация об плате</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </section>


            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
