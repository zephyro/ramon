<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_production">
                <div class="chip_bg bg_01 rot">
                    <img src="img/chip_bg.svg" class="img-fluid" alt="">
                </div>

                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Чип-тюнинг</li>
                    </ul>

                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <br/>
                            <h1>Чип-тюнинг</h1>
                            <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову,</p>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6 col-xl-5 col-xl-offset-1">
                            <div class="main_video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe  src="https://www.youtube.com/embed/9iQL87myauE?rel=0&amp;showinfo=0" frameborder="0" encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="main_page__divider"></div>

                    <div class="form_inline">
                        <div class="form_title">Выберите автомобиль</div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Модель</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 text-center">
                                <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                            </div>
                        </div>
                    </div>

                    <!-- Vendors -->
                    <?php include('inc/vendors.inc.php') ?>
                    <!-- -->

                </div>
            </section>

            <div class="service_block">
                <div class="service_item item_06">
                    <div class="service_item_wrap">
                        <div class="service_content">
                            <h4><span>Выездной чип-тюнинг</span></h4>
                            <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                            <div class="service_block_btn">
                                <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
