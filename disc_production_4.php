<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_production">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Салон Ramon Performance г. Ярославль</li>
                            </ul>
                            <h1>Салон Ramon Performance г. Ярославль</h1>
                        </div>

                    </div>

                    <div class="unit">

                        <div class="row">
                            <div class="col-xs-12 col-lg-6">
                                <h2>Описание салона</h2>
                                <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло. Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами.</p>
                            </div>
                            <div class="col-xs-12 col-lg-6">
                                <div class="unit_gallery">
                                    <div class="unit_slider swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="unit_thumbs">
                                        <li><a href="#" data-target="0"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="1"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="2"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="3"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="4"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="5"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="6"><i class="fa fa-camera"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="builder">
                        <h2 class="text-center">Конструктор кованых дисков</h2>
                        <div class="row">
                            <div class="col-xs-12 col-lg-4 col-xl-3">

                                <div class="builder__step">
                                    <ol>
                                        <li>Выберите дизайн</li>
                                        <li>Выберите комплектацию</li>
                                        <li class="active">Заполните форму</li>
                                    </ol>
                                </div>

                            </div>
                            <div class="col-xs-12 col-lg-8 col-xl-9">

                                <div class="builder__form pb30">
                                    <form class="form">
                                        <div class="form_group">
                                            <input type="text" class="form_control_base" placeholder="Ваше имя">
                                        </div>
                                        <div class="form_group">
                                            <input type="text" class="form_control_base" placeholder="Телефон">
                                        </div>
                                        <div class="form_group">
                                            <input type="text" class="form_control_base" placeholder="E-mail">
                                        </div>
                                        <div class="form_group">
                                            <textarea class="form_control_base" placeholder="Комментарий к заказу" name="" rows="5"></textarea>
                                        </div>
                                        <div class="form_group">
                                            <div class="checkbox_group">
                                                <div class="checkbox_col">
                                                    <div class="checkbox_group_title">Пункт выдачи:</div>
                                                </div>
                                                <div class="checkbox_col">
                                                    <label class="radiobutton">
                                                        <input type="radio" name="nn" value="" checked>
                                                        <div class="radiobutton_text">
                                                            <span>Москва</span>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="checkbox_col">
                                                    <label class="radiobutton">
                                                        <input type="radio" name="nn" value="">
                                                        <div class="radiobutton_text">
                                                            <span>Ярославль</span>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_submit">
                                            <div class="form_submit_captcha">
                                                <img src="images/captcha.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="form_submit_btn">
                                                <button type="submit" class="btn btn_animate btn_red" data-text="Отправить заявку"><span>Отправить заявку</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
