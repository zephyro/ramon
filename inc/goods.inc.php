<div class="goods_block">
    <div class="goods_block__container">
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Колесные дискии комплекты колес</div>
                <img src="images/goods__01.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Выхлопные системы</div>
                <img src="images/goods__02.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Подвеска</div>
                <img src="images/goods__03.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Тормозные механизмы</div>
                <img src="images/goods__04.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Элементы интерьера</div>
                <img src="images/goods__05.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Аэродинамика</div>
                <img src="images/goods__06.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Аксессуары</div>
                <img src="images/goods__07.png" class="img-fluid" alt="">
            </div>
        </a>
        <a href="#" class="goods_block__item">
            <div class="goods_block__elem">
                <div class="goods_block__name">Разное</div>
                <img src="images/goods__08.png" class="img-fluid" alt="">
            </div>
        </a>
    </div>
</div>