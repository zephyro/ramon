<footer class="footer">
    <div class="container">
        <a href="/" class="footer__logo">
            <img src="img/footer__logo.svg" class="img-fluid" alt="">
        </a>
        <ul class="footer__row">
            <li>
                <nav class="footer__nav">
                    <div class="footer__nav_title">Разделы</div>
                    <ul class="footer__nav_menu">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Каталог</a></li>
                        <li><a href="#">Чип-Тюнинг</a></li>
                        <li><a href="#">Услуги</a></li>
                        <li><a href="#">Производство</a></li>
                        <li><a href="#">Контакты</a></li>
                        <li><a href="#">Статьи</a></li>
                    </ul>
                </nav>
            </li>
            <li>
                <nav class="footer__nav">
                    <div class="footer__nav_title">Информация</div>
                    <ul class="footer__nav_menu">
                        <li><a href="#">О компани</a></li>
                        <li><a href="#">Оптовикам</a></li>
                        <li><a href="#">Диллерам</a></li>
                        <li><a href="#">Поставщикам</a></li>
                        <li><a href="#">Партнерам</a></li>
                    </ul>
                </nav>
            </li>
            <li>
                <nav class="footer__nav">
                    <div class="footer__nav_title">Услуги</div>
                    <ul class="footer__nav_menu">
                        <li><a href="#">Выездной чип-тюнинг</a></li>
                        <li><a href="#">Технический осмотор</a></li>
                    </ul>
                </nav>
            </li>
            <li>
                <div class="footer__subscribe">
                    <div class="footer__subscribe_title">Будьте на связи</div>
                    <form class="form">
                        <div class="footer__subscribe_form">
                            <input type="text" class="form_control_sm" name="subscribe" placeholder="Ваш E-mail">
                            <button type="submit" class="btn btn_red btn_sm">Подписаться</button>
                        </div>
                    </form>
                    <div class="footer__subscribe_text">Получайте информациюю о новых поступлениях, акциях и работе Ramon Performance</div>
                </div>
            </li>
        </ul>
        <ul class="footer__bottom">
            <li>Все права защищены Ramon Performance 2018</li>
            <li><a href="#">Разработано студией «Папин Сайт»</a></li>
            <li><a href="#">Sitemap</a></li>
            <li><a href="#">Политика конфиденциальности</a></li>
        </ul>
    </div>
</footer>