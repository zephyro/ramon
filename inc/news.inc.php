<div class="news">
    <div class="container">
        <div class="news__heading">Статьи</div>
        <div class="news__wrap">
            <div class="news__slider swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="news__item">
                            <a href="#" class="news__item_image">
                                <img src="images/news__image.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="news__item_title"><a href="#">Длинное название текста вдве строки не помещае....</a></div>
                            <div class="news__item_text">Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели </div>
                            <div class="news__item_meta">
                                <div class="news__item_view"><i class="fa fa-eye"></i> <span>8500</span></div>
                                <div class="hews__item_date">20.05.2018</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="news__item">
                            <a href="#" class="news__item_image">
                                <img src="images/news__image.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="news__item_title"><a href="#">Длинное название текста вдве строки не помещае....</a></div>
                            <div class="news__item_text">Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели </div>
                            <div class="news__item_meta">
                                <div class="news__item_view"><i class="fa fa-eye"></i> <span>8500</span></div>
                                <div class="hews__item_date">20.05.2018</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="news__item">
                            <a href="#" class="news__item_image">
                                <img src="images/news__image.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="news__item_title"><a href="#">Длинное название текста вдве строки не помещае....</a></div>
                            <div class="news__item_text">Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели </div>
                            <div class="news__item_meta">
                                <div class="news__item_view"><i class="fa fa-eye"></i> <span>8500</span></div>
                                <div class="hews__item_date">20.05.2018</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="news__item">
                            <a href="#" class="news__item_image">
                                <img src="images/news__image.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="news__item_title"><a href="#">Длинное название текста вдве строки не помещае....</a></div>
                            <div class="news__item_text">Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели </div>
                            <div class="news__item_meta">
                                <div class="news__item_view"><i class="fa fa-eye"></i> <span>8500</span></div>
                                <div class="hews__item_date">20.05.2018</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"><i class="fa fa-chevron-right"></i></div>
            <div class="swiper-button-prev"><i class="fa fa-chevron-left"></i></div>
        </div>
    </div>
</div>