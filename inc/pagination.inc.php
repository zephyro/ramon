<div class="pagination_mobile">
    <a href="#" class="btn btn_red_border">Показать еще</a>
</div>

<div class="pagination">
    <ul class="pagination__nav">
        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
        <li><a href="#">1</a></li>
        <li><span>2</span></li>
        <li><b>...</b></li>
        <li><a href="#">6</a></li>
        <li><a href="#">7</a></li>
        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
    </ul>
    <div class="pagination__text">Показано <strong>20 товаров</strong> из 120</div>
    <div class="pagination__view">
        <div class="pagination__view_label">Товаров на странице: </div>
        <div class="pagination__view_select">
            <select class="select_control">
                <option value="20" selected>20</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
    </div>
</div>