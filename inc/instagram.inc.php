<div class="instagram">
    <div class="container">
        <div class="instagram__title">
            <div class="instagram__title_logo"><i class="fa fa-instagram"></i> <span>Instagram</span></div>
            <a href="#" class="instagram__title_link">@ramon_performance</a>
        </div>
        <div class="instagram__row">
            <div class="instagram__item">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm hidden-md hidden-lg">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm hidden-md hidden-lg">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm hidden-md hidden-lg">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
            <div class="instagram__item hidden-xs hidden-sm hidden-md hidden-lg">
                <a href="#">
                    <img src="images/insta_image.jpg" class="img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</div>