<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://fonts.googleapis.com/css?family=Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic" rel="stylesheet">
<link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
<link rel="stylesheet" href="js/vendor/swiper/css/swiper.min.css">
<link rel="stylesheet" href="js/vendor/jquery.scrollbar/jquery.scrollbar.css">
<link rel="stylesheet" href="js/vendor/ion.rangeSlider/css/ion.rangeSlider.css">
<link rel="stylesheet" href="js/vendor/ion.rangeSlider/css/ion.rangeSlider.skinHTML5.css">
<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">


<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/main.css">