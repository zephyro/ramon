<div class="brand">
    <div class="brand__title">Бренды-поставщики комплектующих</div>
    <div class="brand__container">

        <div class="brand__slider swiper-container">

            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="brand__item">
                        <div class="item_image">
                            <img src="images/brand__logo.png" class="img-fluid" alt="">
                        </div>
                        <span class="item_name">Lamborgini</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"><i class="fa fa-chevron-right"></i></div>
        <div class="swiper-button-prev"><i class="fa fa-chevron-left"></i></div>
    </div>
</div>