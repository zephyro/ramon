<header class="header">
    <div class="container">
        <div class="header__row">
            <a class="header__toggle nav_toggle" href="#"><i class="fa fa-bars"></i></a>
            <a class="header__logo" href="/">
                <img src="img/logo.svg" class="img-fluid" alt="">
            </a>

            <nav class="header__nav">
                <span class="header__nav_close nav_toggle"><i class="fa fa-close"></i></span>
                <div class="header__nav_wrap">
                    <ul class="header__nav_menu">
                        <li><a href="#"><span>Каталог</span></a></li>
                        <li class="active"><a href="#"><span>Чип-тюнинг</span></a></li>
                        <li><a href="#"><span>Производство</span></a></li>
                        <li><a href="#"><span>Услуги</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                        <li><a href="#"><span>Салоны</span></a></li>
                        <li><a href="#"><span>Статьи</span></a></li>
                    </ul>
                    <ul class="header__nav_info">
                        <li>
                            <a class="header__phone" href="tel:+79036468000">
                                <span class="phone_icon"><i class="fa fa-phone"></i></span>
                                <span class="phone_text">8-903-646-8000</span>
                            </a>
                        </li>
                        <li>
                            <div class="header__address">
                                <span class="address_icon"><i class="fa fa-home"></i></span>
                                <span class="address_text">г. Москва, ул. Наташи Ковшовой 4</span>
                            </div>
                        </li>
                        <li>
                            <a class="header__cart" href="#">
                                <div class="cart_icon">
                                    <span class="cart_icon_value">5</span>
                                    <span class="cart_icon_inner"><i class="fa fa-shopping-cart"></i></span>
                                </div>
                                <span class="cart_text">Корзина</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="header__nav_social">
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                    </ul>
                </div>
            </nav>
            <div class="header__nav_layout nav_toggle"></div>

            <ul class="header__info">
                <li>
                    <a class="header__phone" href="tel:+79036468000">
                        <span class="phone_icon"><i class="fa fa-phone"></i></span>
                        <span class="phone_text">8-903-646-8000</span>
                    </a>
                </li>
                <li>
                    <div class="header__address">
                        <span class="address_icon"><i class="fa fa-home"></i></span>
                        <span class="address_text">г. Москва, ул. Наташи Ковшовой 4</span>
                    </div>
                </li>
                <li>
                    <a class="header__cart" href="#">
                        <div class="cart_icon">
                            <span class="cart_icon_value">5</span>
                            <span class="cart_icon_inner"><i class="fa fa-shopping-cart"></i></span>
                        </div>
                        <span class="cart_text">Корзина</span>
                    </a>
                </li>
            </ul>

            <div class="header__search">
                <a class="header__search_toggle" href="#"><i class="fa fa-search"></i></a>
                <div class="header__search_form">
                    <form class="form">
                        <input type="text" value="" placeholder="Поиск">
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>