<div class="actions">
    <div class="container">

        <div class="actions__row">

            <!-- Mobile view -->
            <div class="hide-md">
                <div class="actions__item item_01">
                    <h4><span>Подарочный сертификат</span></h4>
                    <a href="#" class="btn btn_white">Подробнее</a>
                </div>
                <div class="actions__item item_02">
                    <h4><span>Скидка 20%<br/> на все выхлопные системы</span></h4>
                    <p></p>
                    <a href="#" class="btn btn_white">Подробнее</a>
                </div>
            </div>

            <!-- Tablet and Desktop view -->
            <div class="hide-xs-only hide-sm-only">
                <div class="swiper-container actions-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="actions__item item_01">
                                <h4><span>Подарочный сертификат</span></h4>
                                <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="actions__item item_02">
                                <h4><span>Скидка 20%<br/> на все выхлопные системы</span></h4>
                                <p></p>
                                <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="actions__item item_03">
                                <h4><span>Выезд на<br/> тушино</span></h4>
                                <p></p>
                                <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next actions-button-next"><i class="fa fa-chevron-right"></i></div>
                <div class="swiper-button-prev actions-button-prev"><i class="fa fa-chevron-left"></i></div>
            </div>

        </div>
    </div>
</div>
