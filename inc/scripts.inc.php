<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
<script src="js/vendor/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="js/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
<script src="js/vendor/svg4everybody.legacy.min.js"></script>
<script src="js/vendor/swiper/js/swiper.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>