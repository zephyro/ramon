<section class="content">
    <div class="container">
        <h1>СЕО ТЕКСТ</h1>
        <h2>An Introduction</h2>
        <p>This little baseline has (almost) all of the content flow and phrasing elements. It attempts to use all of those elements according to their defined semantics. It also tries to provide a baseline style for those elements. So this isn't a normalize or a reset, but maybe the first set of styles you'd add before you start branding stuff. Here is the full list of the elements.</p>
        <br/>
        <br/>
        <h2>Here is a collection of headings</h2>
        <h3>Here is a collection of headings</h3>
        <h4>The heading that (probably) explains sections of content.</h4>
        <br/>
        <br/>
        <h3>Paragraphs and Styles</h3>
        <div class="lead">Text Level Semantics</div>
        <p>I'm that paragraph with some emphasis on the text-level semantics where I might feel the need to share some strong opinions. This paragraph even uses elements that should have been deprecated, but instead the W3C redefined them. And that's dumb, because formerly presentational elements now do silly things like call a thing to your attention for no good reason, or tell you that something is mispelled, or the name of a boat — which makes as much sense as building another Titanic. It makes no sense, but … c'est la vie.</p>
    </div>
</section>