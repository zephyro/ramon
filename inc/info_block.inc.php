<div class="info_block">
    <div class="info_block__bg">
        <div class="info_block__bg_image">
            <img src="img/group__bg.png" class="img-fluid" alt="">
        </div>
    </div>
    <ul class="info_block__content">
        <li>
            <div class="elem_text">
                <div class="elem_text_wrap">
                    <p>Ramon Performance – это то, что подчеркнет дух твоего автомобиля уникальными дизайнерскими решениями и сделает его узнаваемым, ведь настоящий автомобиль должен притягивать взгляды окружающих. Автомобиль – отражение своего владельца, именно поэтому он должен быть запоминающимся и обладать своей харизмой.</p>
                </div>
            </div>
            <div class="elem_image">
                <div class="elem_image_wrap">
                    <img src="img/auto_image_1.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </li>
        <li>
            <div class="elem_text">
                <div class="elem_text_wrap">
                    <p>Мощный автомобиль не должен прятать свою силу, и тюнинг выпустит всю мощь на свободу, улучшит его динамику и отладит работу внутренних систем авто, убрав «задумчивость», медлительность и неуверенность. Теперь езда будет острой, яркой и приносящей удовольствие!</p>
                </div>
            </div>
            <div class="elem_image">
                <div class="elem_image_wrap">
                    <img src="img/auto_image_2.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </li>
        <li>
            <div class="elem_text">
                <div class="elem_text_wrap">
                    <p>Как образ человека не завершен без хорошей обуви, так и твой автомобиль не будет законченным со стоковыми дисками. Ему подберут кованные диски, довершив маскулинный образ, который теперь не останется незамеченным на дороге.</p>
                </div>
            </div>
            <div class="elem_image">
                <div class="elem_image_wrap">
                    <img src="img/auto_image_3.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </li>
        <li>
            <div class="elem_text">
                <div class="elem_text_wrap">
                    <p>Ramon Performance – это стиль, индивидуальность и надежность. Это умение превратить хороший автомобиль в неповторимый. Наше лучшее портфолио – это десятки уникальных автомобилей среди общего потока на улицах города!</p>
                    <div class="elem_text_signature">
                        <img src="img/signature.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <div class="elem_image">
                <div class="elem_image_wrap">
                    <img src="img/auto_image_4.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </li>
    </ul>
</div>