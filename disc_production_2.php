<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_production">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Салон Ramon Performance г. Ярославль</li>
                            </ul>
                            <h1>Салон Ramon Performance г. Ярославль</h1>
                        </div>

                    </div>

                    <div class="unit">

                        <div class="row">
                            <div class="col-xs-12 col-lg-6">
                                <h2>Описание салона</h2>
                                <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло. Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами.</p>
                            </div>
                            <div class="col-xs-12 col-lg-6">
                                <div class="unit_gallery">
                                    <div class="unit_slider swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="unit_thumbs">
                                        <li><a href="#" data-target="0"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="1"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="2"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="3"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="4"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="5"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="6"><i class="fa fa-camera"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="builder">
                        <h2 class="text-center">Конструктор кованых дисков</h2>
                        <div class="row">
                            <div class="col-xs-12 col-lg-4 col-xl-3">

                                <div class="builder__step">
                                    <ol>
                                        <li class="active">Выберите дизайн</li>
                                        <li>Выберите комплектацию</li>
                                        <li>Заполните форму</li>
                                    </ol>
                                </div>

                            </div>
                            <div class="col-xs-12 col-lg-8 col-xl-9">

                                <div class="builder__constructor disabled">
                                    <div class="constructor">
                                        <div class="constructor_result">
                                            <div class="constructor_result_image">
                                                <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="constructor_result_next">
                                                <a href="#" class="btn btn_red btn_constructor_next">Продолжить <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="constructor_data">
                                            <div class="constructor_data_list">
                                                <div class="data_scroll">
                                                    <div class="data_scroll_content">
                                                        <ul class="constructor_list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="" checked>
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="constructor_data_button">
                                                <a href="#"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="constructor_disable">
                                        <a href="#">Вернуться к стандартному дизайну</a>
                                    </div>
                                </div>

                                <div class="builder__upload">
                                    <div class="builder__upload_text">
                                        <h5>Либо загрузите свой вариант дизайна</h5>
                                        Подойдут:<br/>
                                        <ul class="list_point">
                                            <li>Фотографии, где четко виден узор диска</li>
                                            <li>Схема диска</li>
                                            <li>Эскиз диска от руки</li>
                                        </ul>
                                    </div>
                                    <div class="builder__upload_content">
                                        <div class="upload">
                                            <div class="upload_content">
                                                <div class="upload_image">
                                                    <img src="images/whell.jpg" class="img-fluid" alt="">
                                                    <div class="upload_content_name">koleso.jpg</div>
                                                    <a href="images/whell_lg.jpg" class="upload_content_link btn-modal"><i class="fa fa-search-plus"></i></a>
                                                    <span class="upload_content_remove"><i class="fa fa-close"></i></span>
                                                </div>
                                            </div>
                                            <div class="upload_action">
                                                <div class="upload_action_label">Доступные форматы: jpg, png, gif, pdf</div>
                                                <label class="btn btn_dark_border btn_file">
                                                    <input type="file" name="">
                                                    <span>Загрузить заново</span>
                                                </label>
                                                <br/>
                                                <a href="#" class="btn btn_red">Продолжить <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
