<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>Назад к каталогу</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li><a href="#">Каталог</a></li>
                                <li><a href="#">Mersedes-Benz</a></li>
                                <li><a href="#">GLE 350 D</a></li>
                                <li><a href="#">Выхлопные системы</a></li>
                                <li>Audi RS 7 Sportback (C7) 2017</li>
                            </ul>

                            <h1>Каталог</h1>
                        </div>

                    </div>

                    <div class="main_filter">
                        <div class="form_title">Фильтрация каталога</div>
                        <div class="main_filter_wrap">
                            <div class="row">
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Марка</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Модель</option>
                                            <option value="GLE-500">GLE-500</option>
                                            <option value="SLK-500">SLK-500</option>
                                            <option value="GL-200">GL-200</option>
                                            <option value="GLA">GLA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Категория</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Бренд</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col text-center">
                                    <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product">

                        <div class="product__content">

                            <div class="product__heading">
                                <div class="product__heading_text">
                                    <h1>Audi RS 7 Sportback (C7) 2017</h1>
                                    <div class="product__heading_subtitle">Evolution Line (Titanium) S-AU/TI/4H</div>
                                </div>
                                <div class="product__heading_logo">
                                    <img src="images/products_logo.png" class="img-fluid" alt="">
                                </div>
                            </div>

                            <div class="product__gallery">
                                <div class="product__gallery_slider">
                                    <div class="swiper-gallery swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <a href="images/products_image.jpg" class="btn-modal">
                                                    <img src="images/products_image.jpg" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a href="images/products_image.jpg" class="btn-modal">
                                                    <img src="images/products_image.jpg" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="embed-responsive embed-responsive-4by3">
                                                    <iframe src="https://www.youtube.com/embed/s9pxouRT08E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <ul class="product__gallery_thumbs">
                                    <li>
                                        <a href="#" data-target="0">
                                            <img src="images/products_image_th.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="1">
                                            <img src="images/products_image_th.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="2">
                                            <img src="images/video.jpg" class="img-fluid" alt="">
                                            <i class="fa fa-play"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="product__purchase">
                                <div class="product__purchase_price">2 500 000 Р</div>
                                <ul class="product__purchase_sell">
                                    <li><button class="btn btn_red_border">В корзину <i class="fa fa-shopping-cart"></i></button></li>
                                    <li>
                                        <div class="product__availability">
                                            <a class="product__availability_link" href="#">В наличии</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="product__colors">
                                <div class="product__title">Цвет 1</div>
                                <ul class="product_color">
                                    <li>
                                        <label class="product_black">
                                            <input type="radio" name="color" value="">
                                            <span class=""></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_gray">
                                            <input type="radio" name="color" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_red">
                                            <input type="radio" name="color" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_yellow">
                                            <input type="radio" name="color" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_green">
                                            <input type="radio" name="color" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_black_yellow">
                                            <input type="radio" name="color" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_black_red">
                                            <input type="radio" name="color" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                </ul>

                                <div class="product__title">Цвет 2</div>
                                <ul class="product_color">
                                    <li>
                                        <label class="product_black">
                                            <input type="radio" name="color2" value="">
                                            <span class=""></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_gray">
                                            <input type="radio" name="color2" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_red">
                                            <input type="radio" name="color2" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_yellow">
                                            <input type="radio" name="color2" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_green">
                                            <input type="radio" name="color2" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_black_yellow">
                                            <input type="radio" name="color2" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="product_black_red">
                                            <input type="radio" name="color2" value="">
                                            <span></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>

                            <div class="product__picking">

                                <div class="product__title">Комплектация 1</div>
                                <ul class="picking">
                                    <li>
                                        <label>
                                            <input type="radio" name="picking1" value="" checked>
                                            <span>Комплектация 1.1</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="picking1" value="">
                                            <span>Комплектация 1.2</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="picking1" value="">
                                            <span>Комплектация 1.3</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="picking1" value="">
                                            <span>Комплектация 1.4</span>
                                        </label>
                                    </li>
                                </ul>

                                <div class="product__title">Комплектация 2</div>
                                <ul class="picking">
                                    <li>
                                        <label>
                                            <input type="radio" name="picking2" value="" checked>
                                            <span>Комплектация 1.1</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="picking2" value="">
                                            <span>Комплектация 1.2</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="picking2" value="">
                                            <span>Комплектация 1.3</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="picking2" value="">
                                            <span>Комплектация 1.4</span>
                                        </label>
                                    </li>
                                </ul>

                            </div>

                        </div>

                        <div class="product__description">
                            <div class="row">
                                <div class="col-xs-12 col-xl-6">
                                    <h2>Описание товара</h2>
                                    <p>
                                        Audi’s high-performance RS models are an ideal match for the expertly styled and beautifully engineered Akrapovič Evolution exhaust systems.<br/>
                                        The Evolution gives more power, higher torque, decreases the weight, and delivers an unmistakable Akrapovič sound—which has a deep sound at lower revs and a ‘popping’ sound as things speed up when the throttle is lifted.<br/>
                                        This titanium exhaust system has cast collectors and three mufflers, with the central one featuring an x-piece inside, which increase power and improve its sound.
                                    </p>
                                </div>
                                <div class="col-xs-12 col-xl-6">
                                    <h2>Технические характеристики</h2>
                                    <table class="table">
                                        <tr>
                                            <th>Название</th>
                                            <th>Значение</th>
                                        </tr>
                                        <tr>
                                            <td>Характеристка 1</td>
                                            <td>120</td>
                                        </tr>
                                        <tr>
                                            <td>Характеристка 2</td>
                                            <td>200</td>
                                        </tr>
                                        <tr>
                                            <td>Характеристка 3</td>
                                            <td>300</td>
                                        </tr>
                                        <tr>
                                            <td>Характеристка 4</td>
                                            <td>400</td>
                                        </tr>
                                        <tr>
                                            <td>Характеристка 5</td>
                                            <td>500</td>
                                        </tr>
                                        <tr>
                                            <td>Характеристка 6</td>
                                            <td>600</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="product__compatibility">
                            <h2>Совместимость</h2>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_audi">Аudi</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_audi">Аudi</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_mercedes">Mercedes</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_mercedes">Mercedes</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_audi">Аudi</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_audi">Аudi</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_mercedes">Mercedes</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="compatibility">
                                        <div class="compatibility__title compatibility_mercedes">Mercedes</div>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                        <p>A1, A2, A3, A4, A5, A6, A7</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="rec">
                        <div class="h2">Сопутствующие товары</div>
                        <div class="rec__content">
                            <div class="showcase__slider swiper-container">

                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="showcase-button-next swiper-button-next"><i class="fa fa-chevron-right"></i></div>
                                <div class="showcase-button-prev swiper-button-prev"><i class="fa fa-chevron-left"></i></div>

                            </div>
                        </div>
                    </div>

                    <div class="information hide-xs-only hide-sm-only hide-md-only">
                        <div class="h2">Информация</div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-xl-4">
                                <a href="#" class="information__item">
                                    <div class="information__text">Как заказать</div>
                                    <div class="information__icon">
                                        <i class="fa fa-question"></i>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-6 col-xl-4">
                                <a href="#" class="information__item">
                                    <div class="information__text">Доставка</div>
                                    <div class="information__icon">
                                        <i class="fa fa-truck"></i>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-6 col-xl-4">
                                <a href="#" class="information__item">
                                    <div class="information__text">Самовывоз</div>
                                    <div class="information__icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-6 col-xl-4">
                                <a href="#" class="information__item">
                                    <div class="information__text">Салоны<br/>Ramon Performance</div>
                                    <div class="information__icon">
                                        <i class="fa fa-map"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </section>


            <div class="service_block">
                <div class="service_item item_02">
                    <div class="service_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_03">
                    <div class="service_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
