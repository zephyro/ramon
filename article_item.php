<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Статья</li>
                            </ul>

                            <h1>Статья</h1>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-lg-8 col-xl-8">

                            <div class="intro">
                                <h2>An Introduction</h2>
                                <p>This little baseline has (almost) all of the content flow and phrasing elements. It attempts to use all of those elements according to their defined semantics. It also tries to provide a baseline style for those elements. So this isn't a normalize or a reset, but maybe the first set of styles you'd add before you start branding stuff. Here is the full list of the elements.</p>
                                <br/>
                                <h2>Here is a collection of headings</h2>
                                <br/>
                                <br/>
                                <h3>The subheading that should tell you more about it.</h3>
                                <br/>
                                <br/>
                            </div>

                            <div class="main">
                                <h3>Text Level Semantics</h3>
                                <p>I'm that paragraph with some emphasis on the text-level semantics where I might feel the need to share some strong opinions. This paragraph even uses elements that should have been deprecated, but instead the W3C redefined them. And that's dumb, because formerly presentational elements now do silly things like call a thing to your attention for no good reason, or tell you that something is mispelled, or the name of a boat — which makes as much sense as building another Titanic. It makes no sense, but … c'est la vie.</p>

                                <h3>Editing semantics</h3>
                                <p>This paragraph is all about editing my opinions. Sometimes I have opinions that are no longer relevant. Sometimes I mark or highlight some text so that people notice it. Sometimes I insert some thoughts later. Sometimes I delete those thoughts. The user can also select text, so it's important to be sure that the user can discern whether I've highlighted something, or they have.</p>

                                <h3>Definitions</h3>
                                <p>The whole point of this paragraph is meaning. Sometimes we need an explanation, or a Definition . For those times, you have an element that you can use to tell the user that one word in this paragraph is the term that the paragraph is actually explaining.</p>
                                <p>Sometimes, we have to define an abbreviation. Take, LASER, which is an acronym for Light Amplification by Stimulated Emition of Radiation. The abbreviation tags don't make much sense unless they have a title, though.</p>
                                <p>You know what time it is? No, not Howdy-Doody time. It's April 29, 2016.</p>
                                <br/>

                                <h3>Quoting, Citing, scripting</h3>
                                <p>You know how you're saying something, and then you just feel the need to make quick quote, like Hey, I totally want to buy Somnaderpaphil LG for my herpes? But after some reading,you see that small print that says it can give 1 out of 2 users irritable bowel syndrome. Then you read all of scientific things (1/0) or citations 1 that make you feel better.</p>
                                <blockquote>
                                    <p>Go, Blockquote. Quote away. Be the blockquote you've always wanted to be. But within reason. Also, don't forget who made you...</p>
                                    <div class="text-right">
                                        <i>The Dude who spoke you into existence</i>
                                    </div>
                                </blockquote>

                                <br/>

                                <h3>Code Semantics</h3>
                                <p>Writing code is hard. You have stuff like variables. Imagine n is a variable. Maybe you have some code, like n = 1. Now you want to increment it with your + on the keyboard, so the result is a sample (e.g.n == 2 ). And maybe there's data, where maybe the browser needs an internal value, but the user needs to see something friendly.</p>

                                <h3>List semantics</h3>
                                <ol>
                                    <li>The first item in an ordered list</li>
                                    <li>The second item in an ordered list</li>
                                </ol>

                                <ul>
                                    <li>The first item in an ordered list</li>
                                    <li>The second item in an ordered list</li>
                                </ul>


                            </div>

                        </div>
                        <div class="col-xs-12 col-lg-4 col-xl-4">
                            <div class="side_box">
                                <div class="article__nav">
                                    <div class="h3">Другие статьи</div>
                                    <ul>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                        <li><a href="#">The Second Unimportant heading</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="side_box">
                                <div class="side_box_title">
                                    <div class="h3">Собственное производство</div>
                                </div>

                                <div class="side_product side_product_one">
                                    <div class="side_product_title">Обвесы</div>
                                    <a href="#" class="btn btn_animate btn_dark_border" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>

                                <div class="side_product side_product_two">
                                    <div class="side_product_title">Колесные диски</div>
                                    <a href="#" class="btn btn_animate btn_dark_border" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>
                            </div>

                            <div class="side_box">
                                <div class="actions__item item_01">
                                    <h4><span>Подарочный сертификат</span></h4>
                                    <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>
                            </div>

                            <div class="side_box">
                                <div class="actions__item item_02">
                                    <h4><span>Скидка 20%<br/> на все выхлопные системы</span></h4>
                                    <p>с 1 марта по 1 апреля</p>
                                    <a href="#" class="btn btn_animate btn_white" data-text="Подробнее"><span>Подробнее</span></a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>


            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
