<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="offer">
                <div class="container">
                    <div class="offer__content">
                        <h1><span>Тюнинг-ателье класса Люкс</span></h1>
                        <div class="offer__text">Ramon Performance – это тюнинг-ателье, которое вдохнет новую жизнь в твой автомобиль, раскроет его характер и позволит дышать полными оборотами!</div>
                        <ul class="offer__buttons">
                            <li><a class="btn btn_animate btn_red" href="#" data-text="Каталог"><span>Каталог</span></a></li>
                            <li><a class="btn btn_animate btn_white_border" href="#" data-text="Адреса салонов"><span>Адреса салонов</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="cat_block">
                <div class="container">
                    <div class="cat_block__title">Каталог для вашего автомобиля</div>
                    <form class="form">
                        <ul class="cat_block__form">
                            <li>
                                <select class="form_select">
                                    <option value="Mercedes-Benz">Mercedes-Benz</option>
                                    <option value="Volkswagen">Volkswagen</option>
                                    <option value="BMW">BMW</option>
                                    <option value="Audi">Audi</option>
                                </select>
                            </li>
                            <li>
                                <select class="form_select">
                                    <option value="GLE-500">GLE-500</option>
                                    <option value="SLK-500">SLK-500</option>
                                    <option value="GL-200">GL-200</option>
                                    <option value="GLA">GLA</option>
                                </select>
                            </li>
                            <li>
                                <button type="submit" class="btn btn_animate btn_red btn_send" data-text="Показать"><span>Показать</span></button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>

            <!-- Goods -->
            <?php include('inc/goods.inc.php') ?>
            <!-- -->

            <div class="vendor">
                <div class="vendor__title">Основная специализация на марках:</div>
                <div class="vendor__container">
                    <div class="vendor__item">
                        <div class="vendor__logo">
                            <img src="images/logo/logo_01.png" class="img-fluid" alt="">
                        </div>
                        <div class="vendor__name">BMW</div>
                    </div>
                    <div class="vendor__item">
                        <div class="vendor__logo">
                            <img src="images/logo/logo_02.png" class="img-fluid" alt="">
                        </div>
                        <div class="vendor__name">Audi</div>
                    </div>
                    <div class="vendor__item">
                        <div class="vendor__logo">
                            <img src="images/logo/logo_03.png" class="img-fluid" alt="">
                        </div>
                        <div class="vendor__name">Land Rover</div>
                    </div>
                    <div class="vendor__item">
                        <div class="vendor__logo">
                            <img src="images/logo/logo_04.png" class="img-fluid" alt="">
                        </div>
                        <div class="vendor__name">Volkswagen</div>
                    </div>
                    <div class="vendor__item">
                        <div class="vendor__logo">
                            <img src="images/logo/logo_05.png" class="img-fluid" alt="">
                        </div>
                        <div class="vendor__name">Volvo</div>
                    </div>
                    <div class="vendor__item">
                        <div class="vendor__logo">
                            <img src="images/logo/logo_06.png" class="img-fluid" alt="">
                        </div>
                        <div class="vendor__name">Mercedes</div>
                    </div>
                </div>
            </div>

            <div class="service_block">
                <div class="service_item item_01">
                    <div class="service_item_wrap">
                        <div class="service_content">
                            <h4><span>Чип-Тюнинг</span></h4>
                            <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                            <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
                <div class="service_item item_02">
                    <div class="service_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_03">
                    <div class="service_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_04">
                    <div class="service_item_wrap">
                        <div class="service_content">
                            <h4><span>Производство карбоновых обвесов</span></h4>
                            <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                            <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Info Block -->
            <?php include('inc/info_block.inc.php') ?>
            <!-- -->

            <!-- Brand -->
            <?php include('inc/brand.inc.php') ?>
            <!-- -->

            <div class="bnr_block">
                <div class="bnr_item item_01">
                    <div class="bnr_item_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <div class="bnr_item_button">
                            <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
                <div class="bnr_item item_02">
                    <div class="bnr_item_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <div class="bnr_item_button">
                            <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Content -->
            <?php include('inc/content.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
