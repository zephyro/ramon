// --- Top Nav

$(function() {
    var pull = $('.nav_toggle');
    var menu = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('nav_open');
    });
});

// IE11 SVG support
svg4everybody();


$(".btn-modal").fancybox({
    'padding'    : 0
});

// --- Form Select
$('.form_select').selectric({
    maxHeight: 250,
    disableOnMobile: true,
    nativeOnMobile: false,
    responsive: true
});

// --- Form Select
$('.select_white').selectric({
    maxHeight: 250,
    disableOnMobile: true,
    nativeOnMobile: false,
    responsive: true,
    customClass: {
  //      prefix: 'selectric-white'
    }
});



var brand = new Swiper('.brand__slider', {
    slidesPerView: 6,
    spaceBetween: 0,
    init: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },

    breakpoints: {
        1024: {
            slidesPerView: 6
        },
        768: {
            slidesPerView: 4
        },
        576: {
            slidesPerView: 2
        },
        320: {
            slidesPerView: 2
        }
    }

});



var news = new Swiper('.news__slider', {
    slidesPerView: 4,
    spaceBetween: 20,
    init: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },

    breakpoints: {
        1230: {
            slidesPerView: 3,
            spaceBetween: 30
        },
        1024: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 1
        },
        320: {
            slidesPerView: 1
        }
    }

});


var actions = new Swiper('.actions-slider', {
    slidesPerView: 3,
    spaceBetween: 30,
    init: true,
    noSwiping: false,
    navigation: {
        nextEl: '.actions-button-next',
        prevEl: '.actions-button-prev'
    },

    breakpoints: {
        1230: {
            slidesPerView: 2
        },
        1024: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 1
        },
        320: {
            slidesPerView: 1
        }
    }

});


var showcase = new Swiper('.showcase__slider', {
    slidesPerView: 4,
    spaceBetween: 0,
    init: true,
    navigation: {
        nextEl: '.showcase-button-next',
        prevEl: '.showcase-button-prev'
    },

    breakpoints: {
        1230: {
            slidesPerView: 3
        },
        1024: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 1
        },
        320: {
            slidesPerView: 1
        }
    }

});

var myUnit = new Swiper('.unit_slider');

$('.unit_thumbs li a').on('click touchstart', function(event){
    event.preventDefault();

    var index = $(this).attr("data-target");
    console.log(index);
    myUnit.slideTo(index);
});


//

var $range = $(".filter_slider");

$range.ionRangeSlider({
    hide_min_max: true,
    hide_from_to: true
});

$range.on("change", function () {
    var $this = $(this),
        value = $this.prop("value").split(";");

    $this.closest('.form_slider').find('.form_slider__text span').text(value[0] + "-" + value[1]);
});



var gallery = new Swiper('.swiper-gallery', {});

$('.product__gallery_thumbs li a').on('click touchstart', function(event){
    event.preventDefault();

    var index = $(this).attr("data-target");
    console.log(index);
    gallery.slideTo(index);
});


// Input Number

$(function() {
    $('.form_num_down').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.form_num_up').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});

jQuery(document).ready(function(){
    jQuery('.data_scroll').scrollbar();
});