<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page tuning_bg">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Чип-тюнинг</li>
                    </ul>

                    <h1>Чип Тюнинг Mercedes</h1>

                    <div class="form_inline">
                        <div class="form_title">Выберите автомобиль</div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Модель</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Двигатель</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 text-center">
                                <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                            </div>
                        </div>
                    </div>

                    <!-- Models -->
                    <?php include('inc/models.inc.php') ?>
                    <!-- -->
                </div>
            </section>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Content -->
            <?php include('inc/content.inc.php') ?>
            <!-- -->


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
