<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Чек аут</li>
                            </ul>

                            <h1>Чек аут</h1>

                        </div>

                    </div>

                    <div class="cart">
                        <ul class="cart__head">
                            <li>товар</li>
                            <li>цена</li>
                            <li>кол-во</li>
                            <li>подитог</li>
                        </ul>
                        <ul class="cart__row">
                            <li>
                                <a href="#" class="cart_image">
                                    <img src="images/product.jpg" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="cart_name" href="#">Audi RS 7 Sportback (C7) 2017</a>
                                <div class="cart_intro">Текст с описанием</div>
                            </li>
                            <li>
                                <span class="cart_price">2 000 000 Р</span>
                            </li>
                            <li>
                                <div class="form_num">
                                    <span class="form_num_down"><i class="fa fa-minus"></i></span>
                                    <input type="text" name="num" value="1" class="form_num_input">
                                    <span class="form_num_up"><i class="fa fa-plus"></i></span>
                                </div>
                            </li>
                            <li>
                                <span class="cart_price">2 000 000 Р</span>
                            </li>
                            <li>
                                <span class="cart_remove">X</span>
                            </li>
                        </ul>
                        <ul class="cart__row">
                            <li>
                                <a href="#" class="cart_image">
                                    <img src="images/product.jpg" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="cart_name" href="#">Audi RS 7 Sportback (C7) 2017</a>
                                <div class="cart_intro">Текст с описанием</div>
                            </li>
                            <li>
                                <span class="cart_price">2 000 000 Р</span>
                            </li>
                            <li>
                                <div class="form_num">
                                    <span class="form_num_down"><i class="fa fa-minus"></i></span>
                                    <input type="text" name="num" value="1" class="form_num_input">
                                    <span class="form_num_up"><i class="fa fa-plus"></i></span>
                                </div>
                            </li>
                            <li>
                                <span class="cart_price">2 000 000 Р</span>
                            </li>
                            <li>
                                <span class="cart_remove">X</span>
                            </li>
                        </ul>
                        <ul class="cart__row">
                            <li>
                                <a href="#" class="cart_image">
                                    <img src="images/product.jpg" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="cart_name" href="#">Audi RS 7 Sportback (C7) 2017</a>
                                <div class="cart_intro">Текст с описанием</div>
                            </li>
                            <li>
                                <span class="cart_price">2 000 000 Р</span>
                            </li>
                            <li>
                                <div class="form_num">
                                    <span class="form_num_down"><i class="fa fa-minus"></i></span>
                                    <input type="text" name="num" value="1" class="form_num_input">
                                    <span class="form_num_up"><i class="fa fa-plus"></i></span>
                                </div>
                            </li>
                            <li>
                                <span class="cart_price">2 000 000 Р</span>
                            </li>
                            <li>
                                <span class="cart_remove">X</span>
                            </li>
                        </ul>
                        <div class="cart__summary">Итого: 8 000 000 P</div>
                    </div>

                    <div class="cart_divider"></div>

                    <div class="checkout">
                        <h3>Оформление заказа</h3>
                        <form class="form">

                            <div class="checkout_row">

                                <div class="checkout_col">

                                    <div class="checkout_title">Доставка</div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Город">
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Способ доставки">
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Индекс">
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Адрес доставки">
                                    </div>

                                    <div class="checkout_title">Способ оплаты</div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="">
                                    </div>
                                </div>

                                <div class="checkout_col">

                                    <div class="checkout_title">Общая информация</div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Контактный телефон">
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Фамилия, Имя, Отчество">
                                    </div>
                                    <div class="form_group">
                                        <input type="text" class="form_control_base" placeholder="Электронная почта">
                                    </div>
                                    <div class="form_group">
                                        <textarea class="form_control_base" placeholder="Комментарий к заказу" name=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="checkout_text">Мы перезвоним вам в течение суток для уточнения деталей заказа</div>
                            <div class="text-center">
                                <button type="submit" class="btn btn_animate btn_red btn_xl" data-text="Оформить заказ"><span>Оформить заказ</span></button>
                                <div class="checkout_privacy">Оформляя заказ, вы соглашаетесь с условиями политики конфиденциальности</div>
                            </div>

                        </form>
                    </div>

                </div>
            </section>


            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
