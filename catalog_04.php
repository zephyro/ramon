<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>Mersedes-Benz</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li><a href="#">Каталог</a></li>
                                <li>Mersedes-Benz</li>
                            </ul>

                            <h1>Каталог</h1>
                        </div>

                    </div>

                    <div class="main_filter">
                        <div class="form_title">Фильтрация каталога</div>
                        <div class="main_filter_wrap">
                            <div class="row">
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Марка</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Модель</option>
                                            <option value="GLE-500">GLE-500</option>
                                            <option value="SLK-500">SLK-500</option>
                                            <option value="GL-200">GL-200</option>
                                            <option value="GLA">GLA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Категория</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Бренд</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col text-center">
                                    <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <h2>Колесные диски и комплекты колес</h2>

                    <div class="cat_filter hide-xs-only hide-sm-only">
                        <ul class="cat_filter__row">
                            <li>
                                <div class="form_slider">
                                    <div class="form_slider__label">Диаметр диска</div>
                                    <input type="text" class="filter_slider" name="filter_diameter" value="" data-type="double" data-min="10" data-max="20" data-step="1" data-from="16" data-to="20" />
                                    <span class="form_slider__text"><span>16-20</span>'</span>
                                </div>
                            </li>
                            <li>
                                <div class="form_slider">
                                    <div class="form_slider__label">Ширина обода (дюймы)</div>
                                    <input type="text" class="filter_slider" name="filter_rim" value="" data-type="double" data-min="4" data-max="12" data-step="1" data-from="8" data-to="12" />
                                    <span class="form_slider__text"><span>8-12</span>'</span>
                                </div>
                            </li>
                            <li>
                                <div class="form_slider">
                                    <div class="form_slider__label">Количество отверстий</div>
                                    <input type="text" class="filter_slider" name="filter_holes" value="" data-type="double" data-min="2" data-max="10" data-step="1" data-from="4" data-to="8" />
                                    <span class="form_slider__text"><span>4-8</span></span>
                                </div>
                            </li>
                            <li>
                                <div class="form_slider">
                                    <div class="form_slider__label">PCD</div>
                                    <input type="text" class="filter_slider" name="filter_pcd" value="" data-type="double" data-min="100" data-max="130" data-step="1" data-from="105" data-to="120" />
                                    <span class="form_slider__text"><span>105-120</span></span>
                                </div>
                            </li>
                            <li>
                                <div class="form_slider">
                                    <div class="form_slider__label">ET Вылет</div>
                                    <input type="text" class="filter_slider" name="filter_et" value="" data-type="double" data-min="20" data-max="200" data-step="1" data-from="34" data-to="158" />
                                    <span class="form_slider__text"><span>34-158</span></span>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="showcase_row">

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_availability"><span>нет в наличии</span></div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="showcase_col">
                            <div class="goods">
                                <div class="goods_wrap">
                                    <div class="goods_product">
                                        <div class="goods_product_image">
                                            <img src="images/product.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="goods_product_meta">
                                            <span class="product_meta_sale"><b>-30%</b></span>
                                            <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                            <span class="product_meta_new"><b>sale</b></span>
                                        </div>
                                    </div>
                                    <div class="goods_info">
                                        <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                        <div class="goods_price">
                                            <span class="goods_price_old">2 500 000 P</span>
                                            <span class="goods_price_new">2 000 000 Р</span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="goods_mobile_link"></a>
                                <div class="goods_hover">
                                    <ul class="goods_colors">
                                        <li>
                                            <label class="goods_color goods_black">
                                                <input type="radio" name="color" value="">
                                                <span class=""></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_gray">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_green">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_yellow">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="goods_color goods_black_red">
                                                <input type="radio" name="color" value="">
                                                <span></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <ul class="goods_buttons">
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                        </li>
                                        <li>
                                            <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pagination -->
                    <?php include('inc/pagination.inc.php') ?>
                    <!-- -->

                </div>
            </section>


            <div class="service_block">
                <div class="service_item item_02">
                    <div class="service_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_03">
                    <div class="service_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
