<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="service_page page_production">

                <div class="service_bg bg_01 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_1"></use>
                    </svg>
                </div>
                <div class="service_bg bg_02 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_2"></use>
                    </svg>
                </div>
                <div class="service_bg bg_03 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_3"></use>
                    </svg>
                </div>
                <div class="service_bg bg_04 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_4"></use>
                    </svg>
                </div>
                <div class="service_bg bg_05 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_5"></use>
                    </svg>
                </div>
                <div class="service_bg bg_06 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_6"></use>
                    </svg>
                </div>
                <div class="service_bg bg_07 rot">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/service_bg.svg#Layer_7"></use>
                    </svg>
                </div>

                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Услуги</li>
                    </ul>
                    <div class="row">
                        <div class="col-md-10 col-lg-6">
                            <h1>Услуги</h1>
                            <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову,</p>
                        </div>
                    </div>
                </div>
            </section>

            <div class="service_block mb_90">
                <div class="service_item item_01">
                    <div class="service_item_wrap">
                        <div class="service_content">
                            <h4><span>Чип-Тюнинг</span></h4>
                            <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                            <a href="#" class="btn btn_animate btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
                <div class="service_item item_05">
                    <div class="service_item_wrap">
                        <div class="service_content">
                            <h4><span>Техническое обслуживание</span></h4>
                            <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                            <a href="#" class="btn btn_animate" data-text="Подробнее"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Content -->
            <?php include('inc/content.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
