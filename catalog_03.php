<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page">
                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>Mersedes-Benz</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li><a href="#">Каталог</a></li>
                                <li>Mersedes-Benz</li>
                            </ul>

                            <h1>Каталог</h1>
                        </div>

                    </div>

                    <div class="main_filter">
                        <div class="form_title">Фильтрация каталога</div>
                        <div class="main_filter_wrap">
                            <div class="row">
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Марка</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Модель</option>
                                            <option value="GLE-500">GLE-500</option>
                                            <option value="SLK-500">SLK-500</option>
                                            <option value="GL-200">GL-200</option>
                                            <option value="GLA">GLA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Категория</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col">
                                    <div class="form_group">
                                        <select class="select_white">
                                            <option value="Mercedes-Benz" selected>Бренд</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="filter_col text-center">
                                    <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="goods_block">
                <div class="goods_block__container">
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Колесные дискии комплекты колес</div>
                            <img src="images/goods__01.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Выхлопные системы</div>
                            <img src="images/goods__02.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Подвеска</div>
                            <img src="images/goods__03.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Тормозные механизмы</div>
                            <img src="images/goods__04.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Элементы интерьера</div>
                            <img src="images/goods__05.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Аэродинамика</div>
                            <img src="images/goods__06.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Аксессуары</div>
                            <img src="images/goods__07.png" class="img-fluid" alt="">
                        </div>
                    </a>
                    <a href="#" class="goods_block__item">
                        <div class="goods_block__elem">
                            <div class="goods_block__name">Разное</div>
                            <img src="images/goods__08.png" class="img-fluid" alt="">
                        </div>
                    </a>
                </div>
            </div>

            <div class="showcase">
                <div class="container">

                    <div class="showcase__block">
                        <div class="showcase__header">
                            <div class="showcase__header_title">Колесные диски и комплекты колес</div>
                            <div class="showcase__header_button">
                                <a href="#" class="btn"><span>Смотреть все</span> <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <div class="showcase__content">
                            <div class="showcase__slider swiper-container">

                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="showcase-button-next swiper-button-next"><i class="fa fa-chevron-right"></i></div>
                                <div class="showcase-button-prev swiper-button-prev"><i class="fa fa-chevron-left"></i></div>

                            </div>
                        </div>
                    </div>

                    <div class="showcase__block">
                        <div class="showcase__header">
                            <div class="showcase__header_title">Колесные диски и комплекты колес</div>
                            <div class="showcase__header_button">
                                <a href="#" class="btn"><span>Смотреть все</span> <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <div class="showcase__content">
                            <div class="showcase__slider swiper-container">

                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="showcase-button-next swiper-button-next"><i class="fa fa-chevron-right"></i></div>
                                <div class="showcase-button-prev swiper-button-prev"><i class="fa fa-chevron-left"></i></div>

                            </div>
                        </div>
                    </div>

                    <div class="showcase__block">
                        <div class="showcase__header">
                            <div class="showcase__header_title">Колесные диски и комплекты колес</div>
                            <div class="showcase__header_button">
                                <a href="#" class="btn"><span>Смотреть все</span> <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <div class="showcase__content">
                            <div class="showcase__slider swiper-container">

                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="showcase-button-next swiper-button-next"><i class="fa fa-chevron-right"></i></div>
                                <div class="showcase-button-prev swiper-button-prev"><i class="fa fa-chevron-left"></i></div>

                            </div>
                        </div>
                    </div>

                    <div class="showcase__block">
                        <div class="showcase__header">
                            <div class="showcase__header_title">Колесные диски и комплекты колес</div>
                            <div class="showcase__header_button">
                                <a href="#" class="btn"><span>Смотреть все</span> <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <div class="showcase__content">
                            <div class="showcase__slider swiper-container">

                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="goods">
                                            <div class="goods_wrap">
                                                <div class="goods_product">
                                                    <div class="goods_product_image">
                                                        <img src="images/product.jpg" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="goods_product_meta">
                                                        <span class="product_meta_sale"><b>-30%</b></span>
                                                        <span class="product_meta_close"><i class="fa fa-window-close-o"></i></span>
                                                        <span class="product_meta_new"><b>sale</b></span>
                                                    </div>
                                                </div>
                                                <div class="goods_info">
                                                    <div class="goods_name"><span>Очень длинное название в две строки  которое вообще никак не</span></div>
                                                    <div class="goods_price">
                                                        <span class="goods_price_old">2 500 000 P</span>
                                                        <span class="goods_price_new">2 000 000 Р</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="goods_mobile_link"></a>
                                            <div class="goods_hover">
                                                <ul class="goods_colors">
                                                    <li>
                                                        <label class="goods_color goods_black">
                                                            <input type="radio" name="color" value="">
                                                            <span class=""></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_gray">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_green">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_yellow">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="goods_color goods_black_red">
                                                            <input type="radio" name="color" value="">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="goods_buttons">
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_gray" data-text="Подробнее"><span>Подробнее</span></a>
                                                    </li>
                                                    <li>
                                                        <a class="btn btn_animate btn_rect btn_red" data-text="В корзину"><span>В корзину</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="showcase-button-next swiper-button-next"><i class="fa fa-chevron-right"></i></div>
                                <div class="showcase-button-prev swiper-button-prev"><i class="fa fa-chevron-left"></i></div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="service_block">
                <div class="service_item item_02">
                    <div class="service_content">
                        <h4><span>Производство кованных дисков</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
                <div class="service_item item_03">
                    <div class="service_content">
                        <h4><span>Производство карбоновых обвесов</span></h4>
                        <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца.</p>
                        <a href="#" class="btn btn_white_border" data-text="Подробнее"><span>Подробнее</span></a>
                    </div>
                </div>
            </div>

            <!-- Action -->
            <?php include('inc/action.inc.php') ?>
            <!-- -->

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
