<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_production">
                <div class="chip_bg bg_01 rot">
                    <img src="img/chip_bg.svg" class="img-fluid" alt="">
                </div>


                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Выездной чип-тюнинг</li>
                    </ul>

                    <h1>Выездной чип-тюнинг</h1>
                    <p>This little baseline has (almost) all of the content flow and phrasing elements. It attempts to use all of those elements according to their defined semantics. It also tries to provide a baseline style for those elements. So this isn't a normalize or a reset, but maybe the first set of styles you'd add before you start branding stuff. Here is the full list of the elements.</p>

                    <br/>
                    <br/>

                    <h2>An Introduction</h2>
                    <p>This little baseline has (almost) all of the content flow and phrasing elements. It attempts to use all of those elements according to their defined semantics. It also tries to provide a baseline style for those elements. So this isn't a normalize or a reset, but maybe the first set of styles you'd add before you start branding stuff. Here is the full list of the elements.</p>
                    <br/>


                    <div class="center_form">
                        <div class="center_title">Заказать выездной чип-тюнинг</div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Модель</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Двигатель</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Stage</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="name" placeholder="ФИО">
                                    <span class="form_placeholder">ФИО</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="phone" placeholder="Номер телефона">
                                    <span class="form_placeholder">Номер телефона</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <label class="form_radio">
                                        <input type="radio" name="city" value="1" checked>
                                        <span>Москва</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_radio">
                                    <label class="form_label">
                                        <input type="radio" name="city" value="1">
                                        <span>Ярославль</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="address" placeholder="Адрес">
                                    <span class="form_placeholder">Адрес</span>
                                </div>
                            </div>
                        </div>
                        <div class="form_group text-center">
                            <button type="submit" class="btn btn_animate btn_red" data-text="Отправить заявку"><span>Отправить заявку</span></button>
                        </div>
                        <div class="center_form_text">Отправляя заявку, вы соглашаетесь с условиями политики конфиденциальности.</div>
                    </div>

                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
