<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page tuning_bg">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li>Чип-тюнинг</li>
                    </ul>

                    <h1>Чип Тюнинг Mercedes</h1>

                    <div class="form_inline">
                        <div class="form_title">Выберите автомобиль</div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form_group">
                                    <select class="select_white">
                                        <option value="Mercedes-Benz" selected>Модель</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 text-center">
                                <button type="submit" class="btn btn_animate btn_red" data-text="Показать"><span>Показать</span></button>
                            </div>
                        </div>
                    </div>

                    <div class="main_page__divider"></div>

                    <div class="compare">
                        <div class="compare__legend">
                            <div class="compare__legend_title"><span>Параметр</span></div>
                            <ul class="compare__legend_list">
                                <li><span>Объем двигателя</span></li>
                                <li><span>Мощность</span></li>
                                <li><span>Крутящий момент</span></li>
                                <li><span>Макс. скорость</span></li>
                                <li><span>0-100 км/ч</span></li>
                            </ul>
                            <div class="compare__legend_price"><span>Стоимость</span></div>
                        </div>
                        <div class="compare__base">
                            <div class="compare__params">
                                <div class="compare__params_container">
                                    <div class="compare__params_title">Сток</div>
                                    <ul class="compare__params_list">
                                        <li>4663 cm</li>
                                        <li>435 лс</li>
                                        <li>700 нМ</li>
                                        <li>250 км/ч</li>
                                        <li>5.3 сек</li>
                                    </ul>
                                </div>
                                <div class="compare__params_price">$1000</div>
                            </div>
                        </div>
                        <div class="compare__items">
                            <div class="compare-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="compare__params">
                                            <div class="compare__params_container">
                                                <div class="compare__params_title">Stage 1</div>
                                                <ul class="compare__params_list">
                                                    <li>
                                                        <span>4663 cm</span>
                                                        <small></small>
                                                    </li>
                                                    <li>
                                                        <span>435 лс</span>
                                                        <small>+45</small>
                                                    </li>
                                                    <li>
                                                        <span>700 нМ</span>
                                                        <small>+110</small>
                                                    </li>
                                                    <li>
                                                        <span>250 км/ч</span>
                                                        <small>+25</small>
                                                    </li>
                                                    <li>
                                                        <span>5.3 сек</span>
                                                        <small>-0.9</small>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="compare__params_price">по запросу</div>
                                            <div class="compare__params_action">
                                                <a href="#" class="btn btn_animate btn_red" data-text="Заказать"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="compare__params">
                                            <div class="compare__params_container">
                                                <div class="compare__params_title">Stage 2</div>
                                                <ul class="compare__params_list">
                                                    <li>
                                                        <span>4663 cm</span>
                                                        <small></small>
                                                    </li>
                                                    <li>
                                                        <span>435 лс</span>
                                                        <small>+45</small>
                                                    </li>
                                                    <li>
                                                        <span>700 нМ</span>
                                                        <small>+110</small>
                                                    </li>
                                                    <li>
                                                        <span>250 км/ч</span>
                                                        <small>+25</small>
                                                    </li>
                                                    <li>
                                                        <span>5.3 сек</span>
                                                        <small>-0.9</small>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="compare__params_price">по запросу</div>
                                            <div class="compare__params_action">
                                                <a href="#" class="btn btn_animate btn_red" data-text="Заказать"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Navigation -->
                            <div class="swiper-button-prev"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></div>
                            <div class="swiper-button-next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></div>
                        </div>
                    </div>

                    <div class="center_form">
                        <div class="center_title">Заказать выездной чип-тюнинг</div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Марка</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Модель</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Двигатель</option>
                                        <option value="Mercedes-Benz">Mercedes-Benz</option>
                                        <option value="Volkswagen">Volkswagen</option>
                                        <option value="BMW">BMW</option>
                                        <option value="Audi">Audi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <select class="form_select">
                                        <option value="Mercedes-Benz">Stage</option>
                                        <option value="GLE-500">GLE-500</option>
                                        <option value="SLK-500">SLK-500</option>
                                        <option value="GL-200">GL-200</option>
                                        <option value="GLA">GLA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="name" placeholder="ФИО">
                                    <span class="form_placeholder">ФИО</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="phone" placeholder="Номер телефона">
                                    <span class="form_placeholder">Номер телефона</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_group">
                                    <label class="form_radio">
                                        <input type="radio" name="city" value="1" checked>
                                        <span>Москва</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form_radio">
                                    <label class="form_label">
                                        <input type="radio" name="city" value="1">
                                        <span>Ярославль</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form_group">
                                    <input type="text" class="form_control" name="address" placeholder="Адрес">
                                    <span class="form_placeholder">Адрес</span>
                                </div>
                            </div>
                        </div>
                        <div class="form_group text-center">
                            <button type="submit" class="btn btn_animate btn_red" data-text="Отправить заявку"><span>Отправить заявку</span></button>
                        </div>
                        <div class="center_form_text">Отправляя заявку, вы соглашаетесь с условиями политики конфиденциальности.</div>
                    </div>

                </div>
            </section>


            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Content -->
            <?php include('inc/content.inc.php') ?>
            <!-- -->


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script>

            var stage = new Swiper('.compare-slider', {
                slidesPerView: 2,
                spaceBetween: 0,
                init: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },

                breakpoints: {
                    1200: {
                        slidesPerView: 2,
                        spaceBetween: 0
                    },
                    1024: {
                        slidesPerView: 2
                    },
                    768: {
                        slidesPerView: 1
                    },
                    320: {
                        slidesPerView: 1
                    }
                }

            });
        </script>

    </body>
</html>
