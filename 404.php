<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_404">
                <div class="container">
                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>На главную</span>
                        </a>

                    </div>

                    <div class="content_404">
                        <div class="wrap__404">
                            <h1>404</h1>
                            <div class="content_404_subtitle">Страницы не существует</div>
                            <p>От быстрой езды еще никто не умирал. А вот от внезапной остановки...</p>
                            <div class="content_404_button text-center">
                                <a href="#" class="btn btn_white_border" data-text="Вернутся на главную"><span>Вернутся на главную</span></a>
                            </div>

                        </div>
                    </div>

                    <div class="hide-md text-center">
                        <a href="#" class="btn_back">Вернутся на главную</a>
                    </div>

                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
