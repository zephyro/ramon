<!doctype html>
<html class="no-js" lang="">
    <head>
        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->
    </head>
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <ul class="share">
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <section class="main_page page_production">
                <div class="disk_bg bg_01 rot1">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/disk_bg_01.svg#svg_elem_01"></use>
                    </svg>
                </div>
                <div class="disk_bg bg_02 rot2">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/disk_bg_01.svg#svg_elem_02"></use>
                    </svg>
                </div>
                <div class="disk_bg bg_03 rot1">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/disk_bg_01.svg#svg_elem_03"></use>
                    </svg>
                </div>
                <div class="disk_bg bg_04 rot2">
                    <svg class="svg_bg" viewBox="0 0 2400 2400" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/disk_bg_01.svg#svg_elem_04"></use>
                    </svg>
                </div>

                <div class="container">

                    <div class="main_heading">

                        <a href="#" class="main_heading__mobile">
                            <i class="fa fa-chevron-left"></i>
                            <span>Производство кованных диcков</span>
                        </a>

                        <div class="main_heading__desktop">
                            <ul class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li>Производство кованных диcков</li>
                            </ul>
                            <h1>Производство кованных диcков</h1>
                        </div>

                    </div>

                    <div class="unit">

                        <div class="row">
                            <div class="col-xs-12 col-lg-6">
                                <p>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову,</p>
                            </div>
                            <div class="col-xs-12 col-lg-6">
                                <div class="unit_gallery">
                                    <div class="unit_slider swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="images/salon.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="unit_thumbs">
                                        <li><a href="#" data-target="0"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="1"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="2"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="3"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="4"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="5"><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" data-target="6"><i class="fa fa-camera"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="builder">
                        <h2 class="text-center">Конструктор кованых дисков</h2>
                        <div class="row">
                            <div class="col-xs-12 col-lg-4 col-xl-3">

                                <div class="builder__step">
                                    <ol>
                                        <li class="active">Выберите дизайн</li>
                                        <li>Выберите комплектацию</li>
                                        <li>Заполните форму</li>
                                    </ol>
                                </div>

                            </div>
                            <div class="col-xs-12 col-lg-8 col-xl-9">

                                <div class="builder__constructor">
                                    <div class="constructor">
                                        <div class="constructor_result">
                                            <div class="constructor_result_image">
                                                <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="constructor_result_next">
                                                <a href="#" class="btn btn_red btn_constructor_next">Продолжить <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="constructor_data">
                                            <div class="constructor_data_list">
                                                <div class="data_scroll">
                                                    <div class="data_scroll_content">
                                                        <ul class="constructor_list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="" checked>
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="constructor" value="">
                                                                    <i>
                                                                        <img src="images/builder_disk.jpg" class="img-fluid" alt="">
                                                                    </i>
                                                                </label>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="constructor_data_button">
                                                <a href="#"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="builder__upload">
                                    <div class="builder__upload_text">
                                        <h5>Либо загрузите свой вариант дизайна</h5>
                                        Подойдут:<br/>
                                        <ul class="list_point">
                                            <li>Фотографии, где четко виден узор диска</li>
                                            <li>Схема диска</li>
                                            <li>Эскиз диска от руки</li>
                                        </ul>
                                    </div>
                                    <div class="builder__upload_content">
                                        <div class="upload_action">
                                            <br/>
                                            <div class="upload_action_label">Доступные форматы: jpg, png, gif, pdf</div>
                                            <label class="btn btn_red btn_file">
                                                <input type="file" name="">
                                                <span>Загрузить файл</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </section>

            <!-- Instagram -->
            <?php include('inc/instagram.inc.php') ?>
            <!-- -->

            <!-- News -->
            <?php include('inc/news.inc.php') ?>
            <!-- -->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
